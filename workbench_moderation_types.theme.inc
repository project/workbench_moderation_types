<?php

/**
 * @file Theme callbacks.
 */

/**
 * Transforms the states administration form into a reorderable table.
 */
function workbench_moderation_types_admin_states_form($variables) {
  $form = $variables['form'];
  drupal_add_tabledrag('workbench-moderation-states', 'order', 'sibling', 'workbench-moderation-state-weight');
  $header = array(
    t('Name'),
    t('Machine name'),
    t('Description'),
    t('Node types'),
    array('data' => t('Delete'), 'class' => array('checkbox')),
    t('Weight'),
  );

  $rows = array();
  foreach (element_children($form['states']) as $key) {
    $element = &$form['states'][$key];

    $row = array(
      'data' => array(),
      'class' => array('draggable'),
    );
    $row['data']['label'] = drupal_render($element['label']);
    $row['data']['name'] = drupal_render($element['name']) . drupal_render($element['machine_name']);
    $row['data']['description'] = drupal_render($element['description']);
    $row['data']['node_types'] = drupal_render($element['node_types']);
    $row['data']['delete'] = drupal_render($element['delete']);

    $element['weight']['#attributes']['class'] = array('workbench-moderation-state-weight');
    $row['data']['weight'] = drupal_render($element['weight']);

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'workbench-moderation-states')));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Transforms the transitions administration form into a table.
 */
function workbench_moderation_types_admin_transitions_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('From'),
    '',
    t('To'),
    t('Node types'),
    array('data' => t('Delete'), 'class' => array('checkbox')),
  );

  $rows = array();
  foreach (element_children($form['transitions']) as $key) {
    $element = &$form['transitions'][$key];
    $row = array('data' => array());
    $row['data']['from'] = drupal_render($element['from_name']);
    $row['data'][] = '--&gt;';
    $row['data']['to'] = drupal_render($element['to_name']);
    $row['data']['node_types'] = drupal_render($element['node_types']);
    $row['data']['delete'] = drupal_render($element['delete']);
    $rows[] = $row;
  }
  // @TODO: change this css call.
  drupal_add_css(drupal_get_path('module', 'workbench_moderation') . '/css/workbench_moderation.css');
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('width-auto'))));
  $output .= drupal_render_children($form);

  return $output;
}