<?php

/**
 * @file Administration forms
 */

/**
 * Form callback for changing the node types of moderation states
 */
function workbench_moderation_types_states_form($form, $form_state, $state_name) {
  $state = workbench_moderation_types_states($state_name);
  drupal_set_title(t('Change node types for @state', array('@state' => $state->label)));
  $form = array();
  $types = _node_types_build()->types;
  $node_types = array();
  $default_options = array();
  foreach ($types as $type_name => $type) {
    if (workbench_moderation_node_type_moderated($type->type) === TRUE) {
      $node_types[$type->type] = $type->name;
      if (is_array($state->node_types) && $state->node_types[$type->type]) {
        $default_options[] = $type->type;
      }
    }
  }

  $form['state'] = array(
    '#type' => 'hidden',
    '#value' => $state->name,
    );

  $form['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available node types'),
      '#options' => $node_types,
      '#default_value' => $default_options,
    );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update state'),
    );
  return $form;
}

/**
 * Submit callback for updating a state node type
 */
function workbench_moderation_types_states_form_submit($form, $form_state) {
  db_update('workbench_moderation_states')
    ->fields(array('node_types' => serialize($form_state['values']['types'])))
    ->condition('name', $form_state['values']['state'])
    ->execute();
  drupal_goto('admin/config/workbench/moderation/states');
  drupal_set_message(t('Moderation state node types updated'));
}


/**
 * Form callback for changing the node types of moderation states
 */
function workbench_moderation_types_transitions_form($form, $form_state, $transition_name) {
  $transitions = workbench_moderation_types_transitions();
  $transition = $transitions[$transition_name];
  $states = workbench_moderation_types_states();
  drupal_set_title(t('Change node types for @from_name --> @to_name', array('@from_name' => $states[$transition->from_name]->label, '@to_name' => $states[$transition->to_name]->label)));
  $form = array();
  $types = _node_types_build()->types;
  $node_types = array();
  $default_options = array();
  foreach ($types as $type_name => $type) {
    if (workbench_moderation_node_type_moderated($type->type) === TRUE) {
      $node_types[$type->type] = $type->name;
      if (is_array($transition->node_types) && $transition->node_types[$type->type]) {
        $default_options[] = $type->type;
      }
    }
  }

  $form['transition'] = array(
    '#type' => 'hidden',
    '#value' => $transition_name,
    );

  $form['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available node types'),
      '#options' => $node_types,
      '#default_value' => $default_options,
    );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update transition'),
    );
  return $form;
}

/**
 * Submit callback for updating a state node type
 */
function workbench_moderation_types_transitions_form_submit($form, &$form_state) {
  $transitions = workbench_moderation_types_transitions();
  $transition = $transitions[$form_state['values']['transition']];
  db_update('workbench_moderation_transitions')
    ->fields(array('node_types' => serialize($form_state['values']['types'])))
    ->condition('from_name', $transition->from_name)
    ->condition('to_name', $transition->to_name)
    ->execute();
  drupal_goto('admin/config/workbench/moderation/transitions');
  drupal_set_message(t('Moderation transition node types updated'));
}